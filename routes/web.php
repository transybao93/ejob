<?php
use App\Job;
use App\Tag;
use App\job_tag as jt;
use App\Employee;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    //show all data
    // Cache::forget('users');
    // $jobs = Job::take(6)->orderBy('id', 'desc')->get();

    // cache all data from the beginning
    $minutes = 20;
    $jobs = Cache::remember('jobs', $minutes, function(){
        return Job::take(6)->orderBy('id', 'desc')->get();
    });


    return view('default.index')->withJobs($jobs);
})->middleware('checkLocale')->name('main');

// Languages section
Route::get('/lang/{locale}', function ($locale) {
    setLocaleSession($locale);
    return redirect()->back();
});

//Another route
Route::group(['middleware' => ['validID', 'checkLocale']], function () {
    Route::get('/detail', function () {
        checkLanguage();
        return view('default.detail');
    });
});


//php function for multilanguage
function checkLanguage()
{
	if (Session::has('locale')) {
		App::setLocale(Session::get('locale'));
	}
};

function setLocaleSession($locale)
{
	Session::put('locale', $locale);
}

// create resources for job, jobtype, tag, movement
Route::resource('/job', 'JobController');
Route::resource('/type', 'JobTypeController');
Route::resource('/tag', 'TagController');
Route::resource('/movement', 'MovementController');
// Route::resource('/account', 'AccountController');

//Router for search page
Route::get('/search', function () {
    // $jobs = Job::paginate(6);

    //cache data
    $jobs = Cache::remember('all_jobs', 20, function(){
        return Job::paginate(6);
    });
    return view('default.search_list')->withJobs($jobs);

})->middleware('checkLocale');

//Route for Ajax search
Route::post('/s', 'JobController@showBySearchValue');

//Group routing for the authentication user
Route::group(['prefix' => 'user', 'middleware'=>['checkLocale']], function () {
    //register route
    Route::get('/register', function () {
        return view('default.register');
    });

    //get data router
    Route::post('/r/step2', 'AccountController@checkInfo');
    //login route
    Route::get('/login', function ($id) {
        return null;
    });
    Route::resource('/account', 'AccountController');

    //login route
    Route::get('/login', function () {
        return view('default.login');
    });

    //get login data
    Route::post('/checkLogin', 'AccountController@checkLogin');

    //logout
    Route::get('/logout', 'AccountController@logout');

    //Route to the employee profile
    Route::get('/profile/e', function () {
        $accountID = Auth::user()->id;
        $count = Employee::where('account_id', $accountID)->count();
        if($count > 0)
        {
            $emp = Employee::where('account_id', $accountID)->first();
            return view('default.user.employee')->with('emp', $emp);
        }else{
            return view('default.user.employee');
        }
        
        
    })->middleware('checkAuth');

    Route::get('/profile/b', function () {
        return view('default.user.boss');
    })->middleware('checkAuth');

    //update employee information
    Route::post('/profile/e/update', 'EmployeeController@store')->middleware('checkAuth');
    //update recruiter information
    Route::post('/profile/b/update', 'RecruiterController@store')->middleware('checkAuth');

    //view profile
    Route::get('/view/profile', function () {
        return view('default.user.view_profile');
    })->middleware('checkAuth');
});

//Route for ajax checing email
Route::post('/checkEmail', 'AccountController@checkEmail');