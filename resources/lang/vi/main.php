<?php

return [
    'home'=>'Trang chủ',
    'contact'=>'Liên hệ',
    'account'=>'Tài khoản',
    'news'=>'Tin tức',
    'lang'=>'Ngôn ngữ',
    'register'=>'Đăng ký',
    'login' => 'Đăng nhập',
    'reset'=>'Nhập lại',
    'button'=>[
        'viewDetail'=>'Chi tiết',
        'load_more'=> 'Tải thêm',
    ],
];