<?php

return [
    'home'=>'ホーム',
    'contact'=>'コンタック',
    'account'=>'アカウント',
    'news'=>'ニュース',
    'lang'=>'言語',
    'register'=> 'レジスター',
    'login' => 'ログイン',
    'reset'=>'リセット',
    'button'=>[
        'viewDetail'=>'もっと見る',
        'load_more'=> 'もっと見る',
    ],
];