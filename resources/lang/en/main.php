<?php

return [
    'home'=> 'Homepage',
    'contact'=>'Contact',
    'account'=>'Account',
    'news'=>'News',
    'lang'=>'Language',
    'register'=>'Register',
    'login' => 'Login',
    'reset'=>'Reset',
    'button'=>[
        'viewDetail'=>'View more',
        'load_more'=> 'Load more',
    ],
    //Job detail page
    'description' => 'Description',
    'location' => 'Location',
    'nation' => 'Nation',
    'japanese_level' => 'Japanese Level',
    'how_to_go' => 'How to go',
    'job_type' => 'Job Type',
    'job_tag' => 'Tags',
    'job_detail_button' => [
        'apply' => 'Appy now',
    ],
    //Search page
    'search' => 'Search',
    'search_hint' => 'Click Enter to search',
    'search_example_1' => '...',
    'search_example_2' => 'Effective search: each condition is separated by a comma',
    'keywords_supported' => 'List of supported keywords',
    'result' => 'Result(s)',
    //user menu
    'profile' => 'My profile',
    'dashboard' => 'Dashboard',
    'mail_box' => 'Mail box',
    'setting' => 'Settings',
    'logout' => 'Logout',
    //employee profile page
    'emp_first_name' => 'First name',
    'emp_ last_name' => 'Last name',
    'emp_now_living' => 'Living at',
    'emp_nationality' => 'Nationality',
    'emp_cv' => 'Your CV',
    'choose_a_file' => 'Choose a file',
    'max_file_size' => 'Max file size',
    'file_type' => 'Accepted type',
    'emp_update_button' => [
        'save' => 'Save changes',
        'reset' => 'Reset',
    ],

    //register page
    'r_title' => 'Register with',
    'r_step' => '3 steps',
];