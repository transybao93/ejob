@extends('default.template')
@section('title', 'eJobs - Register')
@section('register')
    {{-- css --}}
    <link rel="stylesheet" href="{{ URL::asset('css/default/register.css') }}">
    <div class="uk-column-1-2@m">
        {{-- Main section --}}
            <div class="part1">
                <br>
                 {{-- Errors area --}}
                    @if(count($errors) > 0)
                        <ul>
                            @foreach($errors->all() as $error)
                                <div class="uk-alert-danger" uk-alert>
                                    <a class="uk-alert-close" uk-close></a>
                                    <p>{{$error}}</p>
                                </div>
                            @endforeach
                        </ul>
                    @endif
                {{-- Register form --}}
                <h4 class="uk-heading-divider">Register with <strong>3 easy steps</strong></h4>
                <form method="POST" action="/user/r/step2">
                    {{-- end error area --}}
                    {{csrf_field()}}
                    <div class="uk-inline" style="margin-bottom:10px">
                        <span class="uk-form-icon" uk-icon="icon: user"></span>
                        <input class="uk-input uk-form-width-large" type="text" 
                        name="aName" 
                        placeholder="Your username"
                        required
                        >
                    </div>
                    {{-- <br>
                    <div class="uk-inline" style="margin-bottom:10px">
                        <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
                        <input class="uk-input uk-form-width-large" type="text" placeholder="Your password">
                        <span style="color:red">*</span>
                    </div>
                    <br>
                    <div class="uk-inline">
                        <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
                        <input class="uk-input uk-form-width-large" type="text" placeholder="Confirm password">
                        <span style="color:red">*</span>
                    </div> --}}
                    {{-- Select option area --}}
                    <div class="selectArea">
                        <label class="uk-form-label" for="form-stacked-select">
                            Register as 
                        </label>
                        <div class="uk-form-controls uk-form-width-large">
                            <select class="uk-select" name="aRole" id="form-stacked-select">
                                <option>Vui lòng chọn...</option>
                                <option value="employee">an <strong>Employee</strong> who want to have a job.</option>
                                <option value="boss">a <strong>Boss</strong> who want to hire employees.</option>
                            </select>
                        </div>
                    </div>
                    <div class="selectArea">
                        <label class="uk-form-label" for="form-stacked-select">
                            You are a
                        </label>
                        <div class="uk-form-controls uk-form-width-large">
                            <select class="uk-select" name="aNationality" id="form-stacked-select">
                                <option>Vui lòng chọn...</option>
                                <option value="japanese">日本人</option>
                                <option value="vietnamese">người Việt Nam</option>
                                <option value="other">Others</option>
                            </select>
                        </div>
                    </div>
                    <div class="selectArea">
                        <label class="uk-form-label" for="form-stacked-select">
                            Now living in
                        </label>
                        <div class="uk-form-controls uk-form-width-large">
                            <select class="uk-select" name="aLocation" id="form-stacked-select">
                                <option>Vui lòng chọn...</option>
                                <option value="japan">日本</option>
                                <option value="vietnam">Việt Nam</option>
                                <option value="other">Others</option>
                            </select>
                        </div>
                    </div>     
                    {{-- <br> --}}
                    <button class="uk-button uk-button-primary">@lang('main.register')</button>
                    <button class="uk-button uk-button-default">@lang('main.reset')</button>
                </form>


            </div>
            {{-- End main section --}}

            


        
    </div>

    
@endsection