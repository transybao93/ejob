@extends('default.template')
@section('title', 'Search')
@section('search')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {{-- css + script link --}}
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/default/search_list.css') }}">
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="/js/default/search_list.js"></script>
    <div class="empty"></div>
    {{-- End css + script link --}}
    <h3 class="uk-heading-line" id="basic1"><span>Tìm kiếm công việc</span></h3>
    {{-- Search form --}}
    <nav class="uk-navbar-container" uk-navbar>
        <div class="uk-navbar-left" style="width:100%">

            <div class="uk-navbar-item" style="width:100%">
                <form class="uk-search uk-search-navbar" id="basic" style="width:100%" method="POST" action="/t">
                    {{ csrf_field() }}
                    <span uk-search-icon></span>
                    <input class="uk-search-input input1" 
                    type="search" 
                    placeholder="Tìm kiếm...">
                </form>
            </div>

        </div>
    </nav>
    {{-- End search form --}}
    <i>Nhấn <strong>Enter</strong> để tìm kiếm</i>
    <br>
    <i>Ví dụ: công việc bán thời gian, thời gian từ 7h-9h sáng,...</i>
    <br>
    <i>Cách tìm kiếm hiệu quả: mỗi điều kiện cách nhau bởi dấu "," (phẩy)</i>
    <br>
    {{-- Command list --}}
    <!-- This is an anchor toggling the modal -->
    <a href="#modal-example" uk-toggle>Danh sách các từ khóa hỗ trợ</a>

    <!-- This is the modal -->
    <div id="modal-example" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
            <h2 class="uk-modal-title">Danh sách câu lệnh hỗ trợ</h2>
            <p>
                <ul class="uk-list uk-list-striped">
                    <li>
                        n: điều kiện search.
                        <br>
                        <i>* Tên công việc *</i>
                    </li>
                    <li>
                        dsb: điều kiện search.
                        <br>
                        <i>* Mô tả công việc *</i>
                    </li>
                    <li>
                        c: điều kiện search.
                        <br>
                        <i>* Thành phố mong muốn *</i>
                    </li>
                    <li>
                        l: điều kiện search.
                        <br>
                        <i>* Nơi bạn đang sống *</i>
                    </li>
                </ul>
            </p>
            <p class="uk-text-right">
                <button class="uk-button uk-button-default uk-modal-close" type="button">Đóng</button>
            </p>
        </div>
    </div>
    {{-- End command list --}}
   

    {{-- Result --}}
    <h3 id="result" class="uk-heading-line"><span>Kết quả (<strong id="rnumber">0</strong> kết quả)</span></h3>
    {{-- result list --}}
    <div class="uk-child-width-1-3@m uk-grid-small uk-container bao" uk-grid>
        {{-- check job data --}}
        {{-- Single post --}}
        @if(!$jobs->isEmpty())
            @foreach($jobs as $j)


                <div>
                    <div class="uk-card uk-card-default uk-card-body uk-card-hover">
                        <h3 class="uk-card-title">
                            {{$j->jName}}
                        </h3>
                        <p style="word-break: break-word">
                            {{$j->jDescription}}
                        </p>
                        <p uk-margin>
                            <button class="uk-button uk-button-primary" 
                                    onclick="window.location='{{url('/job/')}}/{{$j->id}}'">
                                @lang('main.button.viewDetail')
                            </button>
                        </p>
                    </div>
                </div>


            @endforeach
        @endif
        {{-- end single post --}}
        <br>  
        
        {{-- <button class="uk-button uk-button-primary" 
                style="margin:0px auto; text-align:center; margin-top: 40px"
                onclick="window.location='{{url('/search')}}'">
            <span uk-icon="icon: more"></span>
            &nbsp;
            @lang('main.button.load_more')
        </button> --}}

    </div>
    {{-- end result list --}}

    {{-- Pagination --}}
    {{$jobs->links()}}
    
        {{-- <div class="pagination">
            <ul class="uk-pagination uk-flex-center" uk-margin>
                <li><a href="#"><span uk-pagination-previous></span></a></li>
                <li><a href="#">1</a></li>
                <li class="uk-disabled"><span>...</span></li>
                <li><a href="#">5</a></li>
                <li><a href="#">6</a></li>
                <li class="uk-active"><span>7</span></li>
                <li><a href="#">8</a></li>
                <li><a href="#"><span uk-pagination-next></span></a></li>
            </ul>
        </div>  --}}
    {{-- End pagination --}}
@endsection