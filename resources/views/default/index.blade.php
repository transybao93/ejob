@extends('default.template')
@section('title', 'eJobs')
@section('index')
{{-- Slider --}}
<ul class="rslides">
    <li><img src="images/slides/tokyo.jpg" alt=""></li>
    <li><img src="images/slides/sakura1.jpg" alt=""></li>
    <li><img src="images/slides/sakura2.jpg" alt=""></li>
</ul>
{{-- End slider --}}

<br>
<h1 class="uk-heading-divider">日本語の仕事</h1>
<br>

{{-- post list --}}
<div class="uk-child-width-1-3@m uk-grid-small uk-container" uk-grid uk-scrollspy="target: > div; cls:uk-animation-fade; delay: 500">
    {{-- check job data --}}
    @if($jobs)
        {{-- check if the job data is exist --}}
        @foreach($jobs as $j)


            {{-- Single post --}}
            <div>
                <div class="uk-card uk-card-default uk-card-body uk-card-hover">
                    <h3 class="uk-card-title">
                        {{$j->jName}}
                    </h3>
                    <p style="word-break: break-word">
                        {{$j->jDescription}}
                    </p>
                    <p uk-margin>
                        <button class="uk-button uk-button-primary" 
                                onclick="window.location='{{url('/job/')}}/{{$j->id}}'">
                            @lang('main.button.viewDetail')
                        </button>
                    </p>
                </div>
            </div>
            {{-- end single post --}}


        @endforeach
    @else
        <p>Non-exist..</p>
    @endif
    
    
    
    <button class="uk-button uk-button-primary" 
            style="margin:0px auto; text-align:center; margin-top: 40px"
            onclick="window.location='{{url('/search')}}'">
        <span uk-icon="icon: more"></span>
        &nbsp;
        @lang('main.button.load_more')
    </button>

</div>

    
@endsection