@extends('default.template')
@section('title', 'Step 2...')
@section('step2')
    {{-- css --}}
    <link rel="stylesheet" href="{{ URL::asset('css/default/register.css') }}">
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="/js/default/step2.js"></script>

    <div class="uk-column-1-2@m">
        {{-- Main section --}}
            <div class="part1">
                <br>
                {{-- Register form --}}
                <h4 class="uk-heading-divider">Step 2 - Almost done</h4>
                <form method="POST" action="/user/account" id="registerForm">
                    {{csrf_field()}}
                    <input type="hidden" name="aName" value="{{$data['name']}}">
                    <input type="hidden" name="aRole" value="{{$data['role']}}">
                    <input type="hidden" name="aNationality" value="{{$data['nationality']}}">
                    <input type="hidden" name="aLocation" value="{{$data['location']}}">


                    <div class="uk-inline" style="margin-bottom:10px">
                        <span class="uk-form-icon" uk-icon="icon: user"></span>
                        <input class="uk-input uk-form-width-large" type="email" id="email" name="aEmail" placeholder="Your email">
                    </div>
                    <br>
                    <div class="uk-inline" style="margin-bottom:10px">
                        <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
                        <input class="uk-input uk-form-width-large" type="password" id="pass" name="aPass" placeholder="Your password">
                    </div>
                    <br>
                    <div class="uk-inline">
                        <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
                        <input class="uk-input uk-form-width-large" type="password" id="passConfirm" placeholder="Confirm password">
                    </div>
                    {{-- Select option area --}}
                    {{-- <div class="selectArea">
                        <label class="uk-form-label" for="form-stacked-select">
                            Register as 
                        </label>
                        <div class="uk-form-controls uk-form-width-large">
                            <select class="uk-select" name="aRole" id="form-stacked-select">
                                <option>Vui lòng chọn...</option>
                                <option value="employee">an <strong>Employee</strong> who want to have a job.</option>
                                <option value="boss">a <strong>Boss</strong> who want to hire employees.</option>
                            </select>
                        </div>
                    </div>
                    <div class="selectArea">
                        <label class="uk-form-label" for="form-stacked-select">
                            You are a
                        </label>
                        <div class="uk-form-controls uk-form-width-large">
                            <select class="uk-select" name="aNationality" id="form-stacked-select">
                                <option>Vui lòng chọn...</option>
                                <option value="japanese">日本人</option>
                                <option value="vietnamese">người Việt Nam</option>
                                <option value="other">Others</option>
                            </select>
                        </div>
                    </div>
                    <div class="selectArea">
                        <label class="uk-form-label" for="form-stacked-select">
                            Now living in
                        </label>
                        <div class="uk-form-controls uk-form-width-large">
                            <select class="uk-select" name="aLocation" id="form-stacked-select">
                                <option>Vui lòng chọn...</option>
                                <option value="japan">日本</option>
                                <option value="vietnam">Việt Nam</option>
                                <option value="other">Others</option>
                            </select>
                        </div>
                    </div>      --}}
                    {{-- <br> --}}
                    <button class="uk-button uk-button-primary">@lang('main.register')</button>
                    <button class="uk-button uk-button-default">@lang('main.reset')</button>
                </form>


            </div>
            {{-- End main section --}}
        </div>
    
@endsection