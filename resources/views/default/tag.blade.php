@extends('default.template')
@section('title', 'eJobs - Tag list')
@section('tag')
    {{-- Breadcrums --}}
    <ul class="uk-breadcrumb">
        <li>
            <a href="/">
                <h4>@lang('main.home')</h4>
            </a>
        </li>
        <li>
            <span href="javascript:void(0)">
                <h4>
                    {{$tName}} - has 
                    <strong>
                        {{$tag->job()->count()}}
                    </strong>
                    post(s)
                </h4>
                
            </span>
        </li>
    </ul>
    {{-- check data list --}}
    @if($tag)
        @if(!$tag->job->isEmpty())     
            @foreach($tag->job as $j)
                {{-- Single post --}}
                <div>
                    <div class="uk-card uk-card-default uk-card-body uk-card-hover">
                        <h3 class="uk-card-title">
                            {{$j->jName}}
                        </h3>
                        <p style="word-break: break-word">
                            {{$j->jDescription}}
                        </p>
                        <p uk-margin>
                            <button class="uk-button uk-button-primary" 
                                    onclick="window.location='{{url('/job/')}}/{{$j->id}}'">
                                @lang('main.button.viewDetail')
                            </button>
                        </p>
                    </div>
                </div>
                <br>
                {{-- end single post --}}
            @endforeach
        @else
            <p>ko tồn tại job</p>
        @endif
    @else
        <p>Ko có data</p>
    @endif
@endsection