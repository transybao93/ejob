@extends('default.template')
@section('title', 'eJobs - Detail Page')
@section('detail')
    {{-- Breadcrums --}}
    <ul class="uk-breadcrumb">
        <li>
            <a href="/">
                <h4>@lang('main.home')</h4>
            </a>
        </li>
        <li>
            <span href="javascript:void(0)">
                <h4>{{$name}}</h4>
                
            </span>
        </li>
    </ul>
    {{-- check data list --}}
    @if($jobs)
    
        @foreach($jobs as $j)
            <dl class="uk-description-list uk-description-list-divider">
                
                
                {{-- Button send CV --}}
                
                <dt>
                    <button class="uk-button uk-button-primary uk-button-large">
                        Send CV to this job !
                    </button>
                </dt>
                {{-- End send CV button --}}


                <dt>
                    <i class="fa fa-file-text-o fa-lg" aria-hidden="true"></i>
                    &nbsp; 
                    Description
                </dt>
                <dd style="word-break:break-word">{{$j->jDescription}}</dd>
                <dt>
                    <i class="fa fa-calendar fa-lg" aria-hidden="true"></i>
                    &nbsp;
                    Start date (prediction)
                </dt>
                <dd>{{$j->jStartDatePrediction}}</dd>
                <dt>
                    <i class="fa fa-map-marker fa-lg" aria-hidden="true"></i>
                    &nbsp;
                    Location
                </dt>
                <dd>{{$j->jLocation}}</dd>
                <dt>
                    <i class="fa fa-globe fa-lg" aria-hidden="true"></i>
                    &nbsp;
                    City
                </dt>
                <dd>{{$j->jCity}}</dd>
                <dt>
                    <i class="fa fa-graduation-cap fa-lg" aria-hidden="true"></i>
                    &nbsp;
                    Japanese Level
                </dt>
                <dd>{{$j->jJapaneseLevel}}</dd>
                {{-- How to go there --}}
                <dt>
                    <i class="fa fa-compass fa-lg" aria-hidden="true"></i>
                    &nbsp;
                    How to go?
                </dt>
                <dd>
                    <br>
                    @if(!$j->movement->isEmpty())
                        @foreach($j->movement as $m)
                            {{$m->mFrom}} - {{$m->mTo}}
                        @endforeach
                    @else
                        <i>no directions</i>
                    @endif
                </dd>
                {{-- End how to go there --}}
                <dt>
                    <i class="fa fa-file-o fa-lg" aria-hidden="true"></i>
                    &nbsp;
                    Jop type
                </dt>
                <dd>
                    {{$j->type->jtName}}
                </dd>

                <dt>
                    <i class="fa fa-tags fa-lg" aria-hidden="true"></i>
                    &nbsp;
                    Tags
                </dt>
                <br>
                <dd>
                    @if(!$j->tag->isEmpty())

                        @foreach($j->tag as $t)
                            <span class="uk-badge" 
                            onclick="window.location='/tag/{{$t->id}}'"
                            style="cursor:pointer">
                                {{$t->tName}}
                            </span>
                            
                        @endforeach

                    @else
                        <i>no tags</i>
                    @endif
                </dd>
                {{-- Button send CV --}}
                
                <dt>
                    <button class="uk-button uk-button-primary uk-button-large">
                        Send CV to this job !
                    </button>
                </dt>
                {{-- End send CV button --}}

            </dl>
        @endforeach  

    @endif
@endsection