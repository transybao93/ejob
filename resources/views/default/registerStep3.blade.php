@extends('default.template')
@section('title', 'Done...')
@section('step2')
    {{-- css --}}
    <link rel="stylesheet" href="{{ URL::asset('css/default/register.css') }}">
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="/js/default/step2.js"></script>

    <div class="uk-column-1-2@m">
        {{-- Main section --}}
            <div class="part1">
                <br>
                {{-- Register form --}}
                <h4 class="uk-heading-divider">Step 3 - Finish</h4>
                <p>Success! Your account has been created!</p>
                <a class="uk-button uk-button-default" href="/user/login">Login now !</a>

            </div>
            {{-- End main section --}}

            


        
    </div>

    
@endsection