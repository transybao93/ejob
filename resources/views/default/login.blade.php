@extends('default.template')
@section('title', 'eJobs - Login')
@section('login')
    {{-- css --}}
    <link rel="stylesheet" href="{{ URL::asset('css/default/login.css') }}">

    <div class="uk-column-1-2@m">
        {{-- Main section --}}
            <div class="part1">
                <br>
                {{-- Register form --}}
                <h4 class="uk-heading-divider">@lang('main.login')</h4>
                <form method="POST" action="/user/checkLogin" id="registerForm">
                    {{csrf_field()}}


                    <div class="uk-inline" style="margin-bottom:10px">
                        <span class="uk-form-icon" uk-icon="icon: user"></span>
                        <input class="uk-input uk-form-width-large" type="email" id="email" name="aEmail" placeholder="Your email">
                    </div>
                    <br>
                    <div class="uk-inline">
                        <span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: lock"></span>
                        <input class="uk-input uk-form-width-large" type="password" id="pass" name="aPass" placeholder="Your password">
                    </div>
                    <div class="uk-margin uk-grid-small uk-child-width-auto">
                        <label><input class="uk-checkbox" type="checkbox" name="chkRemember"> Remember me</label>
                    </div>
                    
                    <button class="uk-button uk-button-primary">@lang('main.login')</button>
                    <button class="uk-button uk-button-default">@lang('main.reset')</button>
                </form>


            </div>
            {{-- End main section --}}

            


        <div class="part2">
            <br>
            <br>
                {{-- Register form --}}
                
                <p></p>
                <div class="ad">
                
                </div>
        </div>
    </div>

    
@endsection