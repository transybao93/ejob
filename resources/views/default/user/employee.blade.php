@extends('default.user.template')
@section('employee')
    <link rel="stylesheet" href="{{ URL::asset('css/default/employee.css') }}">
    {{-- Notification --}}
    <div class="uk-alert-primary" uk-alert>
        <a class="uk-alert-close" uk-close></a>&nbsp;&nbsp;
        <span>Please update your information before get started</span>
    </div>
    {{-- Flash message --}}
    @if(Session::get('flash-message'))
        <div class="uk-alert-success" uk-alert>
            <a class="uk-alert-close" uk-close></a>&nbsp;&nbsp;
            <span>
                {{Session::get('flash-message')}}
            </span>
        </div>
    @endif
    {{-- check data list --}}
    <form method="POST" action="/user/profile/e/update" enctype="multipart/form-data">
        {{ csrf_field() }}
        <table class="uk-table uk-table-divider">
            <input type="hidden" name="aID" value="{{Auth::user()->id}}">


            @if(isset($emp))
            {{-- If has data --}}
                <tbody>
                    <tr>
                        <td>First name</td>
                        <td>
                            <input class="uk-input" name="eFirstName" id="fname" type="text" value="{{$emp->eFirstname}}">  
                        </td>
                    </tr>
                    <tr>
                        <td>Last name</td>
                        <td>
                            <input class="uk-input" type="text" name="eLastName" id="lname" value="{{$emp->eLastname}}">
                        </td>
                    </tr>
                    <tr>
                        <td>Age</td>
                        <td>
                            <input class="uk-input" type="text" name="eAge" id="age" value="{{$emp->eAge}}">
                        </td>
                    </tr>
                    <tr>
                        <td>Now living</td>
                        <td>
                            <input class="uk-input" type="text" id="location" name="eNowLiving" value="{{$emp->eNowLiving}}">
                        </td>
                    </tr>
                    <tr>
                        <td>Nationality</td>
                        <td>
                            <input class="uk-input" type="text" name="eNationality" value="{{$emp->eNationality}}">
                        </td>
                    </tr>
                    {{-- Upload CV area --}}
                    <tr>
                        <td>Your CV</td>
                        <td>
                            <input type="file" name="file" id="file" class="inputfile" />
                            <label for="file" style="margin-bottom:7px">
                                <i class="fa fa-cloud-upload"></i>&nbsp;
                                <span class="lblFile">
                                    Choose a file
                                </span>
                            </label>
                            <br>
                            <i>Max file size: <strong>1MB</strong></i>
                            <br>
                            <i>Accepted type: <strong>DOC, DOCX, PDF</strong></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            
                        </td>
                        <td>
                            <button class="uk-button uk-button-primary" type="submit">Save changes</button>
                            <button class="uk-button uk-button-default">Reset</button>
                        </td>
                    </tr>
                </tbody>
            @else
            {{-- if not have data --}}
                <tbody>
                    <tr>
                        <td>First name</td>
                        <td>
                            <input class="uk-input" name="eFirstName" id="fname" type="text">  
                        </td>
                    </tr>
                    <tr>
                        <td>Last name</td>
                        <td>
                            <input class="uk-input" type="text" name="eLastName" id="lname">
                        </td>
                    </tr>
                    <tr>
                        <td>Age</td>
                        <td>
                            <input class="uk-input" type="text" name="eAge" id="age">
                        </td>
                    </tr>
                    <tr>
                        <td>Now living</td>
                        <td>
                            <input class="uk-input" type="text" id="location" name="eNowLiving">
                        </td>
                    </tr>
                    <tr>
                        <td>Nationality</td>
                        <td>
                            <input class="uk-input" type="text" name="eNationality">
                        </td>
                    </tr>
                    <tr>
                        <td>Nationality</td>
                        <td>
                            <input class="uk-input" type="text" name="eNationality">
                        </td>
                    </tr>
                    {{-- Upload CV area --}}
                    <tr>
                        <td>Your CV</td>
                        <td>
                            <input type="file" name="eCVLink" id="file" class="inputfile" />
                            <label for="file" style="margin-bottom:7px">
                                <i class="fa fa-cloud-upload"></i>&nbsp;
                                <span class="lblFile">
                                    Choose a file
                                </span>
                            </label>
                            <br>
                            <i>Max file size: <strong>1MB</strong></i>
                            <br>
                            <i>Accepted type: <strong>DOC, DOCX, PDF</strong></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            
                        </td>
                        <td>
                            <button class="uk-button uk-button-primary" type="submit">Save changes</button>
                            <button class="uk-button uk-button-default">Reset</button>
                        </td>
                    </tr>
                </tbody>
            @endif
            
        </table>
    </form>

    
    {{-- Scripting --}}
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="/js/default/employee.js"></script>
@endsection