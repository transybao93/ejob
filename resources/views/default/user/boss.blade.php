@extends('default.user.template')
@section('boss')
    {{-- Notification --}}
    <div class="uk-alert-primary" uk-alert>
        <span>Please update your information before get started</span>
        <a href="" style="float:right">Dissmiss</a>
    </div>
    {{-- check data list --}}
    <form method="POST" action="">
        <table class="uk-table uk-table-divider">
            <input type="hidden" name="aID" value="{{Auth::user()->id}}">
            <tbody>
                <tr>
                    <td>First name</td>
                    <td>
                        <input class="uk-input" type="text" name="bFirstName">
                    </td>
                </tr>
                <tr>
                    <td>Last name</td>
                    <td>
                        <input class="uk-input" type="text" name="bLastName">
                    </td>
                </tr>
                <tr>
                    <td>Store name</td>
                    <td>
                        <input class="uk-input" type="text" name="bStoreName">
                    </td>
                </tr>
                <tr>
                    <td>Nationality</td>
                    <td>
                        <input class="uk-input" type="text" name="bNationality">
                    </td>
                </tr>
                <tr>
                    <td>
                        
                    </td>
                    <td>
                        <button class="uk-button uk-button-primary">Update</button>
                        <button class="uk-button uk-button-default">Reset</button>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
    {{-- Scripting --}}
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="/js/boss.js"></script>

@endsection