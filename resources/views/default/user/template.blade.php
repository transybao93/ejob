<!DOCTYPE html>
<html lang="en">
    <head>
        <title>User Profile</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        {{-- UI kits --}}
        {{-- {{ URL::asset('assets/css/bootstrap.min.css') }} --}}
        <link rel="stylesheet" href="{{ URL::asset('uikit/css/uikit.min.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('uikit/css/uikit-rtl.min.css') }}">
        {{-- font awesome --}}
        {{-- <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"/> --}}
        <link rel="stylesheet" href="{{ URL::asset('font-awesome/css/font-awesome.min.css') }}">
    </head>
    <body>
    {{-- {{{ isset(Auth::user()->username) ? Auth::user()->username : Auth::user()->email }}} --}}
    {{-- @if(session()->has('role'))
        <p>
            @if(session()->get('role') == 1)
                <p>Boss</p>
            @endif
            @if(session()->get('role') == 2)
                <p>Employee</p>
            @endif
        </p>
    @endif --}}


    {{-- Container --}}
    <div class="uk-container">
        {{-- Menu --}}
        <nav class="uk-navbar-container" uk-navbar uk-sticky="bottom: #offset">
            <div class="uk-navbar-left">

                <ul class="uk-navbar-nav">
                    <li class="uk-active"><a href="/">User Profile</a></li>
                    <li class="uk-active">
                        <a href="/">
                            <i class="fa fa-home fa-2x" aria-hidden="true"></i>
                            &nbsp;
                            @lang('main.home')
                        </a>
                    </li>
                    <li>
                        <a href="#">....</a>
                        <div class="uk-navbar-dropdown">
                            <ul class="uk-nav uk-navbar-dropdown-nav">
                                <li class="uk-active"><a href="#">Active</a></li>
                                <li><a href="#">Item</a></li>
                                <li class="uk-nav-header">Header</li>
                                <li><a href="#">Item</a></li>
                                <li><a href="#">Item</a></li>
                                <li class="uk-nav-divider"></li>
                                <li><a href="#">Item</a></li>
                            </ul>
                        </div>
                    </li>

                    <li>
                        <a href="javascript:void(0)">
                            <i class="fa fa-newspaper-o fa-2x" aria-hidden="true"></i>
                            {{-- <img src="/images/icons/news.png" alt=""> --}}
                            &nbsp;
                            @lang('main.news')
                        </a>
                    </li>


                    <li>
                        <a href="javascript:void(0)">
                            <i class="fa fa-phone fa-2x" aria-hidden="true"></i>
                            {{-- <img src="/images/icons/contact.png" alt=""> --}}
                            &nbsp;
                            @lang('main.contact')
                        </a>
                    </li>
                    
                </ul>

            </div>
            <div class="uk-navbar-right">

                <ul class="uk-navbar-nav">
                    <li>
                        <a href="#">
                            <i class="fa fa-user-circle fa-2x" aria-hidden="true"></i>
                            {{-- <img src="/images/icons/account.png" alt=""> --}}
                            &nbsp;
                            @if(isset(Auth::user()->username))
                                {{Auth::user()->username}}
                            @else
                                @lang('main.account')
                            @endif
                            
                        </a>
                        <div class="uk-navbar-dropdown">
                            <ul class="uk-nav uk-navbar-dropdown-nav">
                                @if(isset(Auth::user()->username))
                                     <li>
                                        @if(session()->get('role') == 1)
                                            <a href="/user/profile/b">
                                                <i class="fa fa-users" aria-hidden="true"></i>
                                                &nbsp;
                                                Profile(B)
                                            </a>
                                        @elseif(session()->get('role') == 2)
                                            <a href="/user/profile/e">
                                                <i class="fa fa-users" aria-hidden="true"></i>
                                                &nbsp;
                                                Profile(E)
                                            </a>
                                        @endif
                                    </li>
                                     
                                    <li>
                                        <a href="/user/login">
                                            <i class="fa fa-pie-chart" aria-hidden="true"></i>
                                            &nbsp;
                                            Dashboard
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/user/login">
                                            <i class="fa fa-envelope" aria-hidden="true"></i>
                                            &nbsp;
                                            Mail box
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/user/login">
                                            <i class="fa fa-cogs" aria-hidden="true"></i>
                                            &nbsp;
                                            Setting
                                        </a>
                                    </li>
                                    <li class="uk-nav-divider"></li>
                                    <li>
                                        <a href="/user/logout">
                                            <i class="fa fa-sign-out fa-lg" aria-hidden="true"></i>
                                            &nbsp;
                                            Logout
                                        </a>
                                    </li>

                                @else
                                     <li>
                                        <a href="/user/register">
                                            <i class="fa fa-user-o" aria-hidden="true"></i>
                                            &nbsp;
                                            @lang('main.register')
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/user/login">
                                            <i class="fa fa-user" aria-hidden="true"></i>
                                            &nbsp;
                                            @lang('main.login')
                                        </a>
                                    </li>
                                @endif
                               


                                {{-- <li class="uk-active"><a href="#">Active</a></li>
                                <li><a href="#">Item</a></li>
                                <li class="uk-nav-header">Header</li>
                                <li><a href="#">Item</a></li>
                                <li><a href="#">Item</a></li>
                                <li class="uk-nav-divider"></li>
                                <li><a href="#">Item</a></li> --}}


                            </ul>
                        </div>
                    </li>
                    {{-- Multi-language Selection --}}
                    <li>
                        <a href="javascript:void(0)">

                            <i class="fa fa-globe fa-2x" aria-hidden="true"></i>
                            {{-- <img src="/images/flags/lang.png" alt=""> --}}
                            &nbsp;
                            @lang('main.lang')
                        </a>
                        <div class="uk-navbar-dropdown">
                            <ul class="uk-nav uk-navbar-dropdown-nav">
                                <li>
                                    <a href="/lang/vi">
                                        <img src="/images/flags/vietnam.png" alt="">
                                        &nbsp;
                                        Tiếng Việt
                                    </a>
                                </li>
                                <li>
                                    <a href="/lang/en">
                                        <img src="/images/flags/us.png" alt="">
                                        &nbsp;
                                        English
                                    </a>
                                </li>
                                <li>
                                    <a href="/lang/jp">
                                        <img src="/images/flags/japan.png" alt="">
                                        &nbsp;
                                        日本語
                                    </a>
                                </li>


                            </ul>
                        </div>
                    </li>
                    {{-- End multi-language --}}
                </ul>
            </div>
        </nav>
        {{-- End menu --}}
        {{-- Breadcrums --}}
        <ul class="uk-breadcrumb">
            <li>
                <a href="/">
                    <h4>Dashboard</h4>
                </a>
            </li>
            <li class="uk-disabled">
                <span href="javascript:void(0)">
                    <h4>Update information</h4>
                </span>
            </li>
            <li>
                <a href="/user/view/profile">
                    <h4>View Profile</h4>
                </a>
            </li>
            <li>
                <a href="/">
                    <h4>Mail box</h4>
                </a>
            </li>
            <li>
                <a href="/">
                    <h4>Setting</h4>
                </a>
            </li>
        </ul>
        
        {{-- Main section --}}
        @yield('boss')
        @yield('employee')
        @yield('view_profile')
           
        {{-- End main section --}}
        <br>
        <br>
   </div>
   {{-- End Container --}}
    
     <!--Scripting-->
    {{-- {{ URL::asset('assets/css/bootstrap.min.css') --}}
    <script src="/js/jquery-3.2.1.min.js"></script>
    <script src="/uikit/js/uikit.min.js"></script>
    <script src="/uikit/js/uikit-icons.js"></script>
    <script src="/uikit/js/uikit-icons.min.js"></script>
    
    
    
    </body>
</html>