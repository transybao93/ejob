<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompImage extends Model
{
    //
    protected $table='CompImage';
    protected $fillable = ['image_link'];
    protected $guarded = ['id'];

    public function recruiter()
    {
        return $this->belongsTo('App\Recruiter');
    }
}
