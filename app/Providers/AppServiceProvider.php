<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //job repo
        $this->app->singleton(
            \App\Repositories\Job\IJobRepository::class,
            \App\Repositories\Job\JobRepository::class
        );

        //employee repo
        $this->app->singleton(
            \App\Repositories\User\IEmpRepository::class,
            \App\Repositories\User\EmpRepository::class
        );

    }
}
