<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
    protected $table='Tag';
    protected $fillable = [];
    protected $guarded = ['id'];

    public function job()
    {
        return $this->belongsToMany('App\Job');
    }
}
