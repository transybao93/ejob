<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    //
    protected $table='Job';
    protected $fillable = [];
    protected $guarded = ['id'];

    public function type()
    {
        return $this->belongsTo('App\Type');
    }

    public function movement()
    {
        return $this->hasMany('App\Movement');
    }

    public function tag()
    {
        return $this->belongsToMany('App\Tag');
    }

    public function recruiter()
    {
        return $this->belongsTo('App\Recruiter');
    }

    public function employee()
    {
        return $this->belongsToMany('App\Employee');
    }
}
