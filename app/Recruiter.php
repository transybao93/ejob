<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recruiter extends Model
{
    //
    protected $table='Recruiter';
    protected $fillable = [];
    protected $guarded = ['id'];

    public function job()
    {
        return $this->hasMany('App\Job');
    }

    public function account()
    {
        return $this->hasOne('App\Account');
    }
    
    public function imgs()
    {
        return $this->hasMany('App\CompImage');
    }
}
