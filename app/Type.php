<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    //
    protected $table='Type';
    protected $fillable = [];
    protected $guarded = ['id'];

    public function job()
    {
        return $this->hasMany('App\Job');
    }
}
