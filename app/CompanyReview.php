<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyReview extends Model
{
    //
    protected $table='CompanyReview';
    protected $fillable = ['rContent'];
    protected $guarded = ['id'];
}
