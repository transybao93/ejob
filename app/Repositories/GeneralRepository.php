<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model as Model;

use App\Job;

abstract class GeneralRepository implements IGeneralRepository
{
    protected $_model;

    // Get all records
    public function all()
    {
        $this->_model::all();
    }

    //find records by ID type integer
    public function find($id)
    {
        return $this->_model::where('id', $id)->get();
    }

    //create new records with data
    public function create(array $createData)
    {
        return $this->_model::create($createData);
    }

    //update records with data
    public function update(int $id, array $updateData, $column)
    {
        $data = $this->_model::where($column, $id)->first();
        if($data)
        {
            $data->update($updateData);
            return true;
        }
        return false;
    }

    //update records by id with data with job input
    //update 07-08-2017
    public function updateV2(_model $model, array $updateData)
    {
        $id = $model->id;
        $data = $this->find($id);
        if($data)
        {
            $data->update($updateData);
            return true;
        }
        return false;
    }

    //delete records with data
    public function delete(int $id){
        $data = $this->find($id);
        if($data)
        {
            $data->delete();
            return true;
        }
        return false;
    }

    //delete by model input
    //update 07-08-2017
    public function deleteV2(_model $model){
        $id = $model->id;
        $data = $this->find($id);
        if($data)
        {
            $data->delete();
            return true;
        }
        return false;
    }

    //basic model count
    public function countModel($column, $con) :int
    {
        return $this->_model::where($column, $con)->count();
    }

    // basic update or create new model
    // if exist then update model
    // if non-exist then create new model
    public function updateOrCreateModel(int $id, array $updateData, $column)
    {
        return $this->_model::updateOrCreate(
            [$column => $id],
            $updateData
        );
    }



    

}