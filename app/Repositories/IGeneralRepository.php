<?php

namespace App\Repositories;

interface IGeneralRepository{
    /*
     * This interface contains general function for the model
     */
    public function all();
    public function find($id);
    public function create(array $createData);
    public function update(int $id, array $updateData, $column);
    public function delete(int $id);
    public function countModel($column, $con);
    public function updateOrCreateModel(int $id, array $updateData, $column);
}