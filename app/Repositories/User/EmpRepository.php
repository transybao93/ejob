<?php

namespace App\Repositories\User;

use App\Repositories\GeneralRepository;

class EmpRepository extends GeneralRepository implements IEmpRepository
{
    // set model to use
    public function __construct()
    {
        $this->_model = app()->make(\App\Employee::class);
    }

}