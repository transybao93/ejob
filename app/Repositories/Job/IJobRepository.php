<?php

namespace App\Repositories\Job;

interface IJobRepository
{
    // This file contains all the specific action for the job entity
    public function all_byLocation(string $location);
}