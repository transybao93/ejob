<?php

namespace App\Repositories\Job;

use App\Job;

use App\Repositories\GeneralRepository;

class JobRepository extends GeneralRepository  implements IJobRepository
{
    //set data to the _mode variable
    public function __construct()
    {
        //bind Job class to IoC
        $this->_model = app()->make(\App\Job::class);
    }

    //implement from interface
    public function all_byLocation(string $location)
    {
        $r = $this->_model->where('jLocation', $location)->get();
    }

    
}