<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class account_role extends Model
{
    //
    protected $table='account_role';
    protected $fillable = [];
    protected $guarded = ['id'];
}
