<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
    protected $table='Role';
    protected $fillable = [];
    protected $guarded = ['id'];

    public function account()
    {
        return $this->belongsToMany('App\Account');
    }
}
