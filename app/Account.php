<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;

class Account extends Model implements Authenticatable
{
    use AuthenticableTrait;
    //
    protected $table='Account';
    protected $fillable = ['username', 'email', 'password'];
    protected $guarded = ['id'];

    public function role()
    {
        return $this->belongsToMany('App\Role');
    }

    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }

    public function boss()
    {
        return $this->belongsTo('App\Recruiter');
    }
}
