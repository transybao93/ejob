<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use App\Repositories\User\IEmpRepository;

class EmployeeController extends Controller
{
    private $empRepo;
    public function __construct(IEmpRepository $iemp)
    {
        $this->empRepo = $iemp;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //get array of all presence data
        $d = $request->intersect(['eFirstName', 'eLastName', 'eAge', 'eNowLiving', 'eNationality']);
        
        //file part 
        $CV = $request->file('eCVLink');
        // dd(gettype($CV));
        // $CVLink = $CV->getClientOriginalName();
        $CVName = ($CV != NULL)?$CV->getClientOriginalName():'';
        //this link will go to the emp repo to split
        $d['eCVLink'] = $CVName;
        $d['account_id'] = $request->user()->id;
        // dd('total: ' , $d);

        // dd("Những data sẽ insert vào database: " , $d);
        //save the new data
        $accountID = $request->user()->id;

        //có thể sử dụng updateOrCreate
        $this->empRepo->updateOrCreateModel($accountID, $d, 'account_id');
        
        
        //flash session
        $request->session()->flash('flash-message', 'Updated !');
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }
}
