<?php

namespace App\Http\Controllers;

use App\Job;
use Illuminate\Http\Request;
use App\Repositories\Job\IJobRepository;

class JobController extends Controller
{
    private $jobRepo;
    public function __construct(IJobRepository $ijob)
    {
       $this->middleware('checkLocale');
       $this->jobRepo = $ijob;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //show all data
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function show(Job $job)
    {
        $jID = $job->id;
        $jName = $job->jName;
        //show job by id using direct query
        // $jobs = Job::where('id', $jID)->get();
        //show job by id using job repository
        $jobs = $this->jobRepo->find($jID);
        
        return view('default.detail')->with('jobs',$jobs)->withName($jName);

        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function edit(Job $job)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Job $job)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job $job)
    {
        //
    }


    //php function for multilanguage
    public function checkLanguage()
    {
        if (Session::has('locale')) {
            App::setLocale(Session::get('locale'));
        }
    }

    public function setLocaleSession($locale)
    {
        Session::put('locale', $locale);
    }

    //show data with the following
    //tính năng search
    public function showBySearchValue(Request $request)
    {
        $value = $request->input('search_value');
        // return response()->json('ok', 200);
        $data = Job::where('jName', 'LIKE', '%'.$value.'%')->get();
        // $data = Job::where('jName', 'LIKE', '%'.$value.'%')->get();
        return response()->json($data , 200);
        // return view('default.search_list')->withData(json_decode($data));
    }

    //search from cache
    
}
