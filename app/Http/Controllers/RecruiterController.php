<?php

namespace App\Http\Controllers;

use App\Recruiter;
use Illuminate\Http\Request;

class RecruiterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //save recruiter information
        $accountID = Auth::user()->id;
        $firstname = $request->bFirstName;
        $lastname = $request->bLastName;
        $store_anem = $request->bStoreName;
        $nationality = $request->bNationality;

        $count = Employee::where('account_id', $accountID)->count();
        if($count == 0)
        {
            //save data
            $re = new Recruiter();
            $re->rFirstname = $firstname;
            $re->rLastname = $lastname;
            $re->rStoreName = $store_name;
            $re->rNationality = $nationality;
            $re->account_id = $accountID;
            $re->save();
        }else{
            //save data
            $re = Recruiter::where('account_id', $accountID)->first();
            $re->rFirstname = $firstname;
            $re->rLastname = $lastname;
            $re->rStoreName = $store_name;
            $re->rNationality = $nationality;
            $re->account_id = $accountID;
            $re->save();
        }

        
        return 'insert success';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Recruiter  $recruiter
     * @return \Illuminate\Http\Response
     */
    public function show(Recruiter $recruiter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Recruiter  $recruiter
     * @return \Illuminate\Http\Response
     */
    public function edit(Recruiter $recruiter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recruiter  $recruiter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recruiter $recruiter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Recruiter  $recruiter
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recruiter $recruiter)
    {
        //
    }
}
