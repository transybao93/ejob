<?php

namespace App\Http\Controllers;

use App\job_tag as JT;
use App\Job;
use Illuminate\Http\Request;

class JobTagController extends Controller
{
    //

    public function showJobsWithTag(Request $request)
    {
        $tagID = $request->id;
        $joblist = Job::where('id', 2)->get();
        return view('default.tag')->withJoblist($joblist);
    }
}
