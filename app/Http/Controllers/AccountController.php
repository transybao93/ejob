<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Account;
use DB;

class AccountController extends Controller
{
    public function __construct()
    {

    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            // 'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        $this->validate($request, [
            'aEmail' => 'required',
            'aPass' => 'required',
        ]);
        //save data to datanase
        $rID = 0;
        $name = $request->aName;
        $role = $request->aRole;
        if($role == 'boss')
        {
            $rID = 1;
        }elseif($role == 'employee'){
            $rID = 2;
        }
        $nationality = $request->aNationality;
        $location = $request->aLocation;
        $email = $request->aEmail;
        $pass = $request->aPass;
        //check if exist using email
        // $account = Account::firstOrCreate([
        //     'username' => $name,
        //     'email' => $email,
        //     'password' => bcrypt($pass),
        // ]);

        $countAccount = Account::where('email', $email)->count();
        if($countAccount == 0)
        {
            // not exist
            $account  = new Account();
            $account->username = $name;
            $account->email = $email;
            $account->password = bcrypt($pass);
            $account->save();
            $account->role()->attach($rID);
            return view('default.registerStep3');
        }else{
            //existed
            return 'Existed data';
        }
        // $account->role()->attach($rID);
        // return view('default.registerSuccess');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    //function check the information
    // save to flash session data
    public function checkInfo(Request $request)
    {
        //validate
        // $this->validate($request, [
        //     'aName' => 'required|max:10',
        //     'aRole' => 'required',
        // ]);

        $aName = $request->aName;
        $aRole = $request->aRole;
        $aNationality = $request->aNationality;
        $aLocation = $request->aLocation;
        // $data = "Name: " . $aName . " - Role: " . $aRole . " - Nationality: " . $aNationality . " - Location: " . $aLocation;
        $data = [
            'name'=>$aName,
            'role'=>$aRole,
            'nationality'=>$aNationality,
            'location'=>$aLocation,
        ];

        return view('default.loginStep2')->with('data', $data);
    }

    public function checkEmail(Request $request)
    {
        return 0;
    }

    public function checkLogin(Request $request)
    {
        $email = $request->aEmail;
        $pass = $request->aPass;
        $remember = $request->chkRemember;
        $isRemember = true;

        if($remember == 'on')
        {
            $isRemember = true;
        }else{
            $isRemember = false;
        }

        if(Auth::attempt(['email' => $email, 'password' => $pass], $isRemember))
        {
            $uID = Auth::user()->id;
            //set role session data
            $data = DB::table('account_role')->where('account_id', $uID)->first();
            $role = $data->role_id;
            $request->session()->put('role', $role);

            return redirect()->route('main');
            // return $request->session()->get('role');
        }else{
            return 0;
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->back();
    }
}
