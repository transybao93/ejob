<?php

namespace App\Http\Middleware;

use Closure;

class ValidateID
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //check if id is a number or not
        
        return $next($request);
    }
}
