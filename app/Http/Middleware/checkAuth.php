<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class checkAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //check authenticated user
        if(Auth::check())
        {
            return $next($request);
        }
        return redirect()->route('main');
    }
}
