<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavedJob extends Model
{
    //
    protected $table='SavedJob';
    protected $fillable = [];
    protected $guarded = ['id'];
    
}
