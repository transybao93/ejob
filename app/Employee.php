<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    //
    protected $table='Employee';
    protected $fillable = [];
    protected $guarded = ['id'];

    public function job()
    {
        return $this->belongsToMany('App\Job');
    }

    public function account()
    {
        return $this->hasOne('App\Account');
    }
}
