<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movement extends Model
{
    //
    protected $table='Movement';
    protected $fillable = [];
    protected $guarded = ['id'];

    public function job()
    {
        return $this->belongsTo('App\Job');
    }
}
