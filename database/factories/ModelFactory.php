<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

//Command: php artisan test-factory-helper:generate

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Type::class, function(Faker\Generator $faker){
    return [
        'jtName' => $faker->word,
        'jtDescription' => $faker->word,
    ];
});

$factory->define(App\Job::class, function (Faker\Generator $faker) {
    return [
        'jName' => $faker->word,
        'jDescription' => str_random(150),
        'jStartDatePrediction' => $faker->word,
        'jTimeFrom' => $faker->word,
        'jTimeTo' => $faker->word,
        'jDate' => $faker->date(),
        'jDuration' => $faker->randomNumber(),
        'jSalary' => $faker->randomNumber(),
        'jUnit' => $faker->word,
        'jCurrency' => $faker->word,
        'jLocation' => $faker->word,
        'jCity' => $faker->word,
        'jEmployeeRequest' => $faker->word,
        'jJapaneseLevel' => $faker->word,
        'jWorkShift' => $faker->word,
        'jBonus' => $faker->word,
        'jNote' => $faker->word,
        // 'jTypeID' => $faker->randomNumber(),
        'type_id' => factory(App\Type::class)->create()->id,
        'recruiter_id' => factory(App\Recruiter::class)->create()->id,
    ];
});

$factory->define(App\Movement::class, function (Faker\Generator $faker) {
    return [
        'mFrom' => $faker->word,
        'mTo' => $faker->word,
        'mVehicle' => $faker->word,
        'mNote' => $faker->word,
        'job_id' => factory(App\Job::class)->create()->id,
    ];
});

$factory->define(App\Tag::class, function (Faker\Generator $faker) {
    return [
        'tName' => $faker->word,
    ];
});

$factory->define(App\job_tag::class, function (Faker\Generator $faker) {
    return [
        'job_id' => factory(App\Job::class)->create()->id,
        'tag_id' => factory(App\Tag::class)->create()->id,
    ];
});

$factory->define(App\Account::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->userName,
        'email' => $faker->safeEmail,
        'password' => bcrypt($faker->password),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\account_role::class, function (Faker\Generator $faker) {
    return [
        'account_id' => factory(App\Account::class)->create()->id,
        'role_id' => factory(App\Role::class)->create()->id,
    ];
});

$factory->define(App\Employee::class, function (Faker\Generator $faker) {
    return [
        'eFirstname' => $faker->word,
        'eLastname' => $faker->word,
        'eAge' => $faker->word,
        'eNowLiving' => $faker->word,
        'eNationality' => $faker->word,
        //update 26/06/2017
        'eCVLink' => $faker->word,
        'account_id' => factory(App\Account::class)->create()->id,
    ];
});

$factory->define(App\Recruiter::class, function (Faker\Generator $faker) {
    return [
        'rFirstname' => $faker->word,
        'rLastname' => $faker->word,
        'rStoreName' => $faker->word,
        'rNationality' => $faker->word,
        //update 26/06/2017
        'rLocation' => $faker->word,
        'rSize' => $faker->word,
        'rDescription' => $faker->word,
        'note' => $faker->word,
        'rLogoLink' => $faker->word,
        'account_id' => factory(App\Account::class)->create()->id,
    ];
});

$factory->define(App\Role::class, function (Faker\Generator $faker) {
    return [
        'rName' => $faker->word,
        'rDescription' => $faker->word,
    ];
});

$factory->define(App\employee_job::class, function (Faker\Generator $faker) {
    return [
        'employee_id' => factory(App\Employee::class)->create()->id,
        'job_id' => factory(App\Job::class)->create()->id,
    ];
});

$factory->define(App\CompanyReview::class, function (Faker\Generator $faker) {
    return [
        'rContent' => $faker->word,
        'recruiter_id' => factory(App\Recruiter::class)->create()->id,
        'employee_id' => factory(App\Employee::class)->create()->id,
    ];
});

$factory->define(App\CompImage::class, function (Faker\Generator $faker) {
    return [
        'image_link' => $faker->word,
        'recruiter_id' => factory(App\Recruiter::class)->create()->id,
        // 'comp_id' => function () {
        //      return factory(App\Recruiter::class)->create()->id;
        // },
    ];
});

$factory->define(App\SavedJob::class, function (Faker\Generator $faker) {
    return [
        'employee_id' => factory(App\Employee::class)->create()->id,
        'job_id' => factory(App\Job::class)->create()->id,
    ];
});

