<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

     //Command: php artisan test-factory-helper:generate
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        // factory(App\User::class, 50)->create()->each(function ($u) {
        //     $u->posts()->save(factory(App\Post::class)->make());
        // });

        // factory of JobType table
        // factory(App\JobType::class, 10)->create();
        // factory(App\Job::class, 10)->create();

        // factory(App\Movement::class, 10)->create();
        // factory(App\Tag::class, 10)->create();


        //factory of Job table with relationship with Job type table
        // because of job has many tag and otherwise
        // because Job has many movement plan

        // factory(App\Job::class, 10)->create()->each(function($u){
        //     $u->tag()->save(factory(App\Tag::class)->make());
        //     $u->movement()->save(factory(App\Movement::class)->make());
        // });

        // factory(App\account_role::class, 5)->create();
        // factory(App\employee_job::class, 5)->create();
        // factory(App\SavedJob::class, 5)->create();
        // factory(App\CompanyReview::class, 5)->create();
        // factory(App\CompImage::class, 5)->create();
        

    }
}
