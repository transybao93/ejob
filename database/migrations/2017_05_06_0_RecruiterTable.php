<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecruiterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('Recruiter', function (Blueprint $table) {
            $table->increments('id');
            $table->string('rFirstname');
            $table->string('rLastname');
            $table->string('rStoreName');
            $table->string('rNationality');
            // update 26/06/2017
            $table->string('rLocation');
            $table->string('rSize');
            $table->string('rDescription');
            $table->string('note');
            $table->string('rLogoLink');
            $table->timestamps();

            //foreign key
            $table->integer('account_id')->unsigned();
            $table->foreign('account_id')->references('id')->on('Account');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('Recruiter');
    }
}
