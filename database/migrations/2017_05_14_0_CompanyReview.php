<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CompanyReview extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('CompanyReview', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('rContent');
            $table->integer('recruiter_id')->unsigned();
            $table->integer('employee_id')->unsigned();

            $table->timestamps();

            //foreign key
            $table->foreign('recruiter_id')->references('id')->on('Recruiter');
            $table->foreign('employee_id')->references('id')->on('Employee');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('CompanyReview');
    }
}
