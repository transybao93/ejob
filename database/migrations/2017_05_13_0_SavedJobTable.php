<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SavedJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('SavedJob', function (Blueprint $table) {
            //this table using for employee save the job they like
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('employee_id')->unsigned();
            $table->integer('job_id')->unsigned();
            $table->timestamps();

            //foreign key
            $table->foreign('employee_id')->references('id')->on('Employee');
            $table->foreign('job_id')->references('id')->on('Job');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('SavedJob');
    }
}
