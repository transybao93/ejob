<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MovementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('Movement', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('mFrom'); // nơi đi
            $table->string('mTo'); // nơi đến
            $table->string('mVehicle'); // đi bằng phương tiện
            $table->string('mNote'); // ghi chú
            $table->timestamps();

            $table->integer('job_id')->unsigned();
            //foreign key
            $table->foreign('job_id')->references('id')->on('Job');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('Movement');
    }
}
