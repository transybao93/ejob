<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class JobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('Job', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('jName', 200); // tên công việc
            $table->string('jDescription', 200); // mô tả công việc
            $table->string('jStartDatePrediction', 20); // ngày bắt đầu làm việc dự tính (vào 05/06/2017)
            $table->string('jTimeFrom', 20); // làm từ mấy giờ
            $table->string('jTimeTo', 20); // làm đến mấy giờ
            $table->date('jDate'); // làm những ngày nào trong tuần (2-4-6 / 3-5-7 / 7-CN / 5 ngày trong tuần / 6 ngày trong tuần)
            $table->integer('jDuration'); // mấy tiếng 1 ngày (~ 8 tiếng 1 ngày)
            $table->integer('jSalary'); //lương 
            $table->string('jUnit'); //đơn vị tính (/ngày, /tuần, /tháng)
            $table->string('jCurrency'); // đồng tiến (VND hay 円) -> hỗ trợ đổi ra tiền Việt
            $table->string('jLocation'); //địa điểm làm việc (bao gồm số nhà, tên đường, tên quận, tên thành phố/ tỉnh)
            $table->string('jCity'); // Việt Nam hay Nhật Bản?
            $table->string('jEmployeeRequest'); // những yêu cầu dành cho ứng viên (nhanh nhẹn, trung thực,...)
            $table->string('jJapaneseLevel'); // trình độ tiếng Nhật tối thiểu (N5/N4/N3/N2)
            $table->string('jWorkShift'); // chi tiết về các ca làm việc (ca 1 từ mấy giờ đến mấy giờ, ca 2)
            // $table->string('jTag'); // từ khóa công việc (tách bảng)
            $table->string('jBonus'); // ghi chú các nội dung bonus khi đến làm việc
            $table->string('jNote')->nullable(); // ghi chú thêm
            $table->integer('type_id')->unsigned(); // loại công việc (bán thời gian, cộng tác viên, phụ cửa hàng,...) ~ tách bảng
            $table->integer('recruiter_id')->unsigned(); // khóa ngoại của người tuyển dụng
            // $table->string('jHowToGo'); // cách thức di chuyển (tách bảng)
            $table->timestamps();


            //foreign key
            $table->foreign('type_id')->references('id')->on('Type');
            $table->foreign('recruiter_id')->references('id')->on('Recruiter');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('Job');
    }
}
