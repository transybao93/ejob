$(document).ready(function(){
    $('#registerForm').submit(function(e){
        var pass = $('#pass').val();
        var passConfirm = $('#passConfirm').val();
        var email = $('#email').val();

        if(pass === passConfirm)
        {
            // alert('OK');
            $(this).submit();
        }else{
            alert('Your password and password confirm are not match.');
        }

        
        // alert('Email: ' + email);

        // alert('Password: ' + pass + ' - password confirm: ' + passConfirm);

        e.preventDefault();
    });

    function checkEmail()
    {
        //check if email exist or not in real-time
            $dataObject = {email: $('#email').val()};
            //set csrf token
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: '/user/account',
                data: Object($dataObject),
                success:function(data){
                    if(data == 0)
                    {
                        alert('chưa có email này');
                    }else{
                        alert('đã tồn tại email');
                    }
                },
                error: function(xhr, textStatus, error){
                    console.log(xhr.statusText);
                    console.log(textStatus);
                    console.log(error);
                }
            });
    }

});