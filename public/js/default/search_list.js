$(document).ready(function(){

   // toggle the advanced area
   $('.uk-link-muted').bind('click', function(){
        $('.advancedArea').toggle('fast');
   });

   //focus to the input
   $('.input1').focus();
   //scroll to input
    $('html, body').animate({
        scrollTop: $(".empty").offset().top
    }, 1000);

   //basic search form
   $('#basic').submit(function(e){
        var search_value = $('.uk-search-input').val();
        ajaxCall('POST', '/s', {search_value: $('.uk-search-input').val()});
        e.preventDefault();
   });
   

   //this function will return an json
   function ajaxCall($method, $url, $dataObject)
   {
       // data object {search_value: $('.uk-search-input').val()}
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: $method,
            url: $url,
            data: Object($dataObject),
            success:function(data){
                //dùng phân trang trong ajax
                if(data.total != 0)
                {
                    var total = data.total;
                    var per_page = data.per_page;
                    var current = data.current_page;
                    var last = data.last_page;
                    var next_page_url = data.next_page_url;
                    var prev_page_url = data.prev_page_url;
                    var from = data.from;
                    var to = data.to;
                    var d = data.data;
                    
                    //pagination
                    $('.uk-pagination').empty();
                    $('.bao').empty();
                    var li = '';
                    var prev = '<li><a href="#"><span uk-pagination-previous></span></a></li>';
                    var next = '<li><a href="#"><span uk-pagination-next></span></a></li>';
                    //single post
                   
                    var pages = Math.ceil(total/per_page);
                    var isEnd = false;
                    if(total <= per_page) // không còn thằng nào nữa => prev sẽ bị ẩn
                    {
                        isEnd = true;
                    }
                    
                    // alert('total: ' + data.total);
                    // alert('length: ' + d.length);
                    // alert('record per page: ' + Math.ceil(data.total/per_page));
                    for(var i = from; i<= (pages); i++)
                    {
                        if(i == current)
                        {
                            li += '<li class="uk-active"><span>'+ (current) +'</span></li>';
                        }else{
                            li += '<li><a href="' + next_page_url + '">'+(i)+'</a></li>';
                        }
                    }

                    var single_post = '';
                    for (var p in d) {
                        // output += o[p]['jName'] + ' - ';
                        //single post
                        single_post += '<div>'  +
                                            '<div class="uk-card uk-card-default uk-card-body uk-card-hover">' +
                                                '<h3 class="uk-card-title">' + d[p]['jName'] + '</h3>' +
                                               '<p style="word-break: break-word">' +
                                                    d[p]['jDescription'] +
                                                '</p>' +
                                                '<p uk-margin>' +
                                                    '<button class="uk-button uk-button-primary"' +
                                                            'onclick="window.location=">' +
                                                        'detail...' +
                                                    '</button>' +
                                                '</p>' +
                                            '</div>' +
                                        '</div>';
                    }


                    
                    $('.bao').append(single_post);
                    if(isEnd)
                    {
                        $('.uk-pagination').append(prev);
                        $('.uk-pagination').append(li);
                    }else{
                        $('.uk-pagination').append(li);
                        $('.uk-pagination').append(next);
                    }
                    // $('.uk-pagination').append(prev);
                    // $('.uk-pagination').append(li);
                    // $('.uk-pagination').append(next);
                    


                    //scroll to data
                    $('html, body').animate({
                        scrollTop: $("#result").offset().top
                    }, 1000);
                    //set the result count
                    $('#rnumber').html(total);
                }else{
                    alert('Nothing return. Sorry :(');
                }
                //không dùng phân trang mà show ra hết kết quả

                if(data.length != 0)
                {
                    var single_post = '';
                    // analyzeObject(data);
                    for (var p in data) {
                        // output += o[p]['jName'] + ' - ';
                        //single post
                        single_post += '<div>'  +
                                            '<div class="uk-card uk-card-default uk-card-body uk-card-hover">' +
                                                '<h3 class="uk-card-title">' + data[p]['jName'] + '</h3>' +
                                                '<p style="word-break: break-word">' +
                                                    data[p]['jDescription'] +
                                                '</p>' +
                                                '<p uk-margin>' +
                                                    '<a class="uk-button uk-button-primary"' + 
                                                    'href="/job/'+data[p]['id']+'">' +
                                                    'Detail' +
                                                    '</a>' + 
                                                '</p>' +
                                            '</div>' +
                                        '</div>';
                    }

                    //scroll to data
                    $('.bao').append(single_post);
                    $('html, body').animate({
                        scrollTop: $("#result").offset().top
                    }, 1000);
                    //set the result count
                    $('#rnumber').html(data.length);
                }else{
                    return 0;
                }
                
                
            }
        });
        // return null;
   }

});

function analyzeObject(o)
{
    var output = '';
    for (var p in o) {
        output += o[p]['jName'] + ' - ';
    }
    alert(output);
}